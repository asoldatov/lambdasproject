package streams;

import model.Artist;
import model.SampleData;

import java.util.Iterator;
import java.util.List;

public class FilterAndCountExample {

    public Integer countArtistsFromRussiaIteration(List<Artist> artistList){
        int count = 0;
        for (Artist artist : artistList){
            if (artist.getNationality().equals("RU")){
                count++;
            }
        }
        return count;
    }

    public Integer countArtistsFromRussiaExternalIteration(List<Artist> artistList){
        int count = 0;
        Iterator<Artist> artistIterator = artistList.iterator();
        while (artistIterator.hasNext()){
            Artist artist = artistIterator.next();
            if (artist.getNationality().equals("RU")){
                count++;
            }
        }
        return count;
    }

    public Long countArtistsFromRussiaInternalIteration(List<Artist> artistList){
        long count = 0;
        /*count = artistList.stream()
                .filter(artist -> artist.isFrom("RU"))
                .count();
        */

        //Anything printed, because stream is lazy recipe
        /* artistList.stream()
                .filter(artist -> {
                    System.out.println("Artist name = " + artist.getName());
                    return artist.isFrom("RU");
                });
        */

        //If it gives you back a Stream, it’s lazy; if it gives you back another value or void, then it’s
        //eager.

        count = artistList.stream()
                .filter(artist -> {
                   System.out.println("Artist name = " + artist.getName());
                   return artist.isFrom("RU");
                }).count();
        return count;
    }

    public static void main(String[] args){
        FilterAndCountExample filterAndCountExample = new FilterAndCountExample();
        List<Artist> artistList = SampleData.getAllArtists();
        //Integer count = filterAndCountExample.countArtistsFromRussiaIteration(artistList);
        //Integer count = filterAndCountExample.countArtistsFromRussiaExternalIteration(artistList);
        Long count = filterAndCountExample.countArtistsFromRussiaInternalIteration(artistList);
        System.out.println("Count of Russia artists = " + count);
    }
}
