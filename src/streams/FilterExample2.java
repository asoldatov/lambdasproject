package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterExample2 {

    public List<String> filterArray(String[] elementsArray){
        List<String> beginningWithNumbers = new ArrayList<>();
        for (String element : elementsArray){
            if (isDigit(element.charAt(0))){
                beginningWithNumbers.add(element);
            }
        }
        return beginningWithNumbers;
    }

    public List<String> filterArrayWithLambdas(String[] elementArray){
        List<String> resultList = Stream.of(elementArray)
            .filter(element -> isDigit(element.charAt(0)))
            .collect(Collectors.toList());
        return resultList;
    }

    private boolean isDigit(char symbol){
        try {
            Long.parseLong(String.valueOf(symbol));
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public static void main(String[] args){
        FilterExample2 filterExample2 = new FilterExample2();
        String[] elementsArray = {"a", "1abc", "abc1", "2"};
        //List<String> filterArray = filterExample2.filterArray(elementsArray);
        List<String> filterArray = filterExample2.filterArrayWithLambdas(elementsArray);
        filterArray.forEach(element -> {
            System.out.println("Element = " + element);
        });
    }
}
