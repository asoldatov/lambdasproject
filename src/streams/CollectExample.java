package streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectExample {

    public static void main(String[] args){
        List<String> collected = Stream.of("a", "b", "c")
                .collect(Collectors.toList());
        for (String element : collected){
            System.out.println("Element = " + element);
        }
    }
}
