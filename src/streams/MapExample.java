package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapExample {

    public List<String> uppercaseAllElements(String[] elements){
        List<String> collected = new ArrayList<>();
        for (String element : elements){
            String uppercaseString = element.toUpperCase();
            collected.add(uppercaseString);
        }
        return collected;
    }

    public List<String> uppercaseAllElementsWithStream(String[] elements){
        List<String> collected = Stream.of(elements)
                .map(element -> element.toUpperCase())
                .collect(Collectors.toList());
        return collected;
    }

    public static void main(String[] args){
        MapExample mapExample = new MapExample();
        String[] elementsArray = new String[]{"a", "b", "c", "d", "e", "f", "hello"};
        //List<String> elementsList = mapExample.uppercaseAllElements(elementsArray);
        List<String> elementsList = mapExample.uppercaseAllElementsWithStream(elementsArray);
        elementsList.forEach(element -> {
            System.out.println("Element = " + element);
        });
    }
}
